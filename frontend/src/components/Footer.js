export default function Footer() {
    return (
        <footer className="footer">
            <div className="py-1 my-1">
                <p className="footer-paragraph1">&#169; 2022 Gold Diggers PH, Inc.</p>
                <p className="footer-paragraph2"><strong>All products</strong> that are <strong>displayed</strong> on this page are not mine. All copies are with its rightful owners.</p>
            </div>
            <div className="footer-links">
                <a className="footer-links1" href="/register">Terms of Service</a>
                <a className="footer-links1" href="/register">Privacy Policy</a>
                <a className="footer-links1" href="/register">Do Not Sell My Information</a>
            </div>
        </footer>
    )
}