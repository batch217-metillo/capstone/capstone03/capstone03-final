import {useState, useEffect} from "react";
import { Container, Table} from "react-bootstrap";


export default function Cart(){

	const [allUsers, setAllUsers] = useState([])

	const fetchUserOrders = () => {
		fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/user/details`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.orders.map(order => {
				return(
					<tr key={order._id}>
						<td>{order.products[0].productName}</td>
						<td>{order.products[0].quantity}</td>
						<td>{order.totalAmount}</td>
					</tr>
					)
				}));
			});
		}

	useEffect(() => {
		fetchUserOrders();
	}, []);


	return (
		<Container className="mt-5">
		<h2 className="text-center p-4 font-link-bold">Your Shopping Cart</h2>
           <Table striped bordered hover>
			     <thead>
			       <tr>
			         <th>Product Name</th>
			         <th>Quantity</th>
			         <th>Price</th>
			         <th>Sub Total</th>
			         <th>Action</th>
			       </tr>
			     </thead>
			     <tbody>
			       {allUsers}
			     </tbody>
			   </Table>
        </Container>
	)
}

