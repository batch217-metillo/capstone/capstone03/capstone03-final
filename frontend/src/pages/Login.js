import {Carousel, Container, Row, Col, Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link} from "react-router-dom";
import UserContext from '../UserContext.js';
import {Navigate} from 'react-router-dom';
import LSlider1 from "../images/Lslider1.png";
import LSlider2 from "../images/Lslider2.png";
import LSlider3 from "../images/Lslider3.png";
import Swal from 'sweetalert2';

export default function Login() {

    const {user, setUser} = useContext(UserContext);

    const [emailAddress, setEmailAddress] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {
        if(emailAddress !== '' && password !== ''){
            setIsActive(true); 
        }else {
            setIsActive(false);
        }

    }, [emailAddress, password]);

    function authenticate(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/login`, {
          method: "POST",
          headers: {
            "Content-Type" : "application/json"
          },
          body: JSON.stringify({
            emailAddress: emailAddress,
            password: password
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data.access);

          //If no user is found, the "access" property will not be available and will return undefined.
          if(data.access !== undefined){

              localStorage.setItem("token", data.access);
              retrieveUserDetails(data.access);

              Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Gold Diggers PH"
              })
          }else {
              Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Check your login credentials and try again!"
              })
          }
        })

        // localStorage.setItem("emailAddress", emailAddress);

        /*setUser({
          emailAddress: localStorage.getItem("emailAddress")
        })*/

        setEmailAddress('')
        setPassword('')

        // console.log(`${emailAddress} has been verified! Welcome back!`);
    }

    const retrieveUserDetails = (token) => {
        
        fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/user/details`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
          console.log(data);

          setUser({
              id: data._id,
              isAdmin: data.isAdmin
          });
        })
    }

    return (
      (user.id !== null) 
      ?
        <Navigate to="/"/>
      :
        <Container fluid>
              
              <Row>
                <Col xs={12} md={6}>
                  <Carousel className="carousel-banner mt-1">
                      {/*FIRST CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={LSlider1}
                            alt="First slide"
                          />
                        </Carousel.Item>

                      {/*SECOND CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={LSlider2}
                            alt="Second slide"
                          />
                        </Carousel.Item>

                      {/*THIRD CAROUSEL ITEM*/}
                        <Carousel.Item>
                          <img
                            className="d-block w-100"
                            src={LSlider3}
                            alt="Third slide"
                          />
                        </Carousel.Item>
                      </Carousel>
                </Col>

                <Col className="p-4 mt-4" xs={12} md={6}>

                <h4 className="login-title">Login</h4>
                  <Form onSubmit={(e) => authenticate(e)}>

                    {/*EMAIL ADDRESS*/}
                        <Form.Group className="mb-2" controlId="emailAddress">
                          <Form.Label className="login-item">Email Address</Form.Label>
                          <Form.Control className="login-item" type="email" placeholder="name@example.com" 
                          required onChange={e => setEmailAddress(e.target.value)} value={emailAddress}/>
                        </Form.Group>

                    {/*PASSWORD*/}
                        <Form.Group className="mb-2" controlId="password">
                          <Form.Label className="login-item">Password</Form.Label>
                          <Form.Control className="login-item" type="password" placeholder="Password" 
                          required onChange={e => setPassword(e.target.value)} value={password}/>
                        </Form.Group>

                    {/*SUBMIT BUTTON*/}
                        {
                            isActive 
                            ?
                            <Container className="p-2 text-center">
                            <Button className="login-button" variant="secondary" type="submit" id="submitBtn">
                            Login
                            </Button>
                            </Container>
                            :
                            <Container className="p-2 text-center">
                            <Button className="login-button" variant="secondary" type="submit" id="submitBtn" disabled>
                            Login
                            </Button>
                            </Container>
                        }


                    <h6 className="orLine p-3">or</h6>
                    <h6 className="login-using-username pt-3"><i>Login using your Username</i></h6>

                    <Container className="login-button-container pb-3 text-center">
                      <Button className="login-button" variant="secondary" type="submit" id="submitBtn" as={Link} to={`/loginusername`}>
                            Login using your Username
                      </Button>
                    </Container>


                    <p className="text-center">Don't have an account yet? <a href="/register">Register Now!</a></p>



                        
                  </Form>
                </Col>
              </Row>
        </Container>
    )
}