import {Row, Col, Card, Container, Button} from 'react-bootstrap';
import Necklace from "../images/productCardNecklace.png";
import Pendant from "../images/productCardPendant.png";
import Earrings from "../images/productCardEarrings.png";
import Ring from "../images/productCardRing.png";


export default function ProductCardOld() {
	return (
	<>
	<h1 className="our-products"> Product Category </h1>
        <Row className="my-4">

          {/*FIRST CARD*/}
          <Col xs={6} md={3}>
            <Card className="productCards p-3">
              <Card.Img variant="top" src={Necklace} />
              <Card.Body>
                <Card.Title>
                  <h2>NECKLACES</h2>
                </Card.Title>
                <Card.Text>
                  asfgasfljalskfjklasjfkas
                </Card.Text>
                <Button variant="primary">Buy Now!</Button>
              </Card.Body>
            </Card>
          </Col>

          {/*SECOND CARD*/}
          <Col xs={6} md={3}>
            <Card className="productCards p-3">
              <Card.Img variant="top" src={Pendant} />
              <Card.Body>
                <Card.Title>
                  <h2>PENDANTS</h2>
                </Card.Title>
                <Card.Text>
                  asfgasfljalskfjklasjfkas
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>

          {/*THIRD CARD*/}
          <Col xs={6} md={3}>
            <Card className="productCards p-3">
              <Card.Img variant="top" src={Earrings} />
              <Card.Body>
                <Card.Title>
                  <h2>EARRINGS</h2>
                </Card.Title>
                <Card.Text>
                  asfgasfljalskfjklasjfkas
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>

          {/*FOURTH CARD*/}
          <Col xs={6} md={3}>
            <Card className="productCards p-3">
              <Card.Img variant="top" src={Ring} />
              <Card.Body>
                <Card.Title>
                  <h2>RINGS</h2>
                </Card.Title>
                <Card.Text>
                  asfgasfljalskfjklasjfkas
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>

          

        </Row>
    </>
    )
}