import './App.css';
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar.js';
//import AppSidebar from './components/AppSidebar.js';
import Home from './pages/Home.js'
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import LoginUsername from './pages/LoginUsername.js'
import Logout from './pages/Logout.js'
import AdminDashboard from "./pages/AdminDashboard";
import Cart from "./pages/Cart";
import Error from "./pages/Error";
import Footer from './components/Footer.js';
import PreFooter from './components/PreFooter.js';
import {Route, Routes} from 'react-router-dom'
import {BrowserRouter as Router} from 'react-router-dom'
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext.js'
import './App.css';


function App() {

  const [user, setUser] = useState({
    //emailAddress: localStorage.getItem("emailAddress")
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // UseEffect for checking user data
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`${process.env.REACT_APP_ECOMMERCEAPI_URL}/user/details`, {
      method: "POST",
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>

        <Router>
            {/*HEAD*/}
            <AppNavbar />
           {/* <AppSidebar />*/}

            {/*BODY*/}
            <Container>
                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route exact path="/admin" element={<AdminDashboard/>} />
                    <Route exact path="/products/:productId" element={<ProductView />} />
                    <Route exact path="/login" element={<Login />} />
                    <Route exact path="/loginusername" element={<LoginUsername />} />
                    <Route exact path="/register" element={<Register />} />
                    <Route exact path="/logout" element={<Logout />} />
                    <Route exact path="/products" element={<Products />} />
                    <Route exact path="/cart" element={<Cart/>} />
                    <Route path="*" element={<Error/>}/>
                </Routes>
            </Container>

            {/*FOOTER*/}
            <PreFooter/>
            <Footer/>
        </Router>
    </UserProvider>
  );
}

export default App;
