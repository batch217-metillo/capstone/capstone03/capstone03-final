const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	username: {
		type: String,
		required: [true, "Username is required!"]
	},
	emailAddress: {
		type: String,
		required: [true, "Email Address is required!"]
	},
	phoneNumber: {
		type: String,
		required: [true, "Phone Number is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [{
		products: [{
			productId: {
			type: String,
			required: [true, "Product Id is required!"]
			},
			productName: {
			type: String
			},
			quantity: {
				type: Number,
				default: 1
			},
			_id: false,
		}],
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
},
{
	timestamps: true
}
)

module.exports = mongoose.model("User", userSchema);